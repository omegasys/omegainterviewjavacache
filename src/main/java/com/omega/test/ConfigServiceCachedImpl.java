package com.omega.test;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 *
 *
 */
public class ConfigServiceCachedImpl extends ConfigServiceImpl {

    private final Logger log = LoggerFactory.getLogger(this.getClass().getSimpleName());

    private volatile Map<String, String> registryHashStore = new HashMap<String, String>();
    private DateTime lastUpdateTime =  new DateTime();
    private final int cacheExpireInSecond = 5 * 60; //5 min


    public void init() {
        super.init();
        log.info("Initializing ConfigServiceCachedImpl");
        refreshRegistryStores();
        log.info("ConfigServiceCachedImpl init completed.");
    }


    public void refreshRegistryHashStore() {
        log.info("Refreshing Registry Hash Store...");
        Map<String, String> newRegistryHashStore = new HashMap<String, String>();

        this.registryHashStore.clear();
        for (RegistryHash registryHash : getRegistryHashRepository().findAll()) {
            registryHashStore.put(registryHash.getMapkey().toLowerCase(), registryHash.getValue());
        }
        log.info("Registry Hash Store reloaded.");
    }

    public void refreshRegistryStores() {

        log.info("Refreshing registry stores. lastUpdateTime is " + lastUpdateTime);
        DateTime now = new DateTime();

        if (now.plusSeconds(cacheExpireInSecond).isBefore(lastUpdateTime) ){
            refreshRegistryHashStore();
            lastUpdateTime = now;
        }

        log.info("Registry stores reloaded ");
    }

    private boolean isReloadRequired(){
        DateTime now = new DateTime();
        return lastUpdateTime.plusSeconds(cacheExpireInSecond).isBefore(now) ;
    }

    public String getValue(String key) {
        if (isReloadRequired()){
            refreshRegistryStores();
        }
        return super.getValue(key);
    }


    public Integer getIntValue(String key) {
        if (isReloadRequired()){
            refreshRegistryStores();
        }
        return super.getIntValue(key);
    }


    public Boolean getBooleanValue(String key) {
        if (isReloadRequired()){
            refreshRegistryStores();
        }
        return super.getBooleanValue(key);
    }




}
