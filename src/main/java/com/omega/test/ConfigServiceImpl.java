package com.omega.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;


/**
 */
public class ConfigServiceImpl implements ConfigService {

    private final Logger log = LoggerFactory.getLogger(this.getClass().getSimpleName());

    @Autowired
    protected RegistryHashRepository registryHashRepository;


    /**
     * Initialized the service. This is called through spring after the bean has been constructed (see xml config)
     */
    public void init() {
        log.info("Initializing ConfigServiceImpl");

    }

    public String getValue(String key) {
        RegistryHash retVal = registryHashRepository.findByMapkey(key);
        if (retVal == null) {
            return null;
        } else {
            return retVal.getValue();
        }
    }



    public Integer getIntValue(String key) {
        return Integer.parseInt(getValue(key));
    }




    public Boolean getBooleanValue(String key) {
        return Boolean.parseBoolean(getValue(key));
    }


    public void refreshRegistryStores() {
        //do nothing. This level don't implement the caching.
    }

    public RegistryHashRepository getRegistryHashRepository() {
        return registryHashRepository;
    }
}
